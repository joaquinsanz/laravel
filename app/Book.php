<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title','author','editorial_id','cdu']; // para crear y que sepa los campos rellenables

    public function place(){
        return $this->belongsTo('App\Place');
    }

    public function editorial(){
        return $this->belongsTo('App\Editorial');
    }
    public function CDU(){
        return $this->belongsTo('App\Cdu','cdu','cdu');
    }
}
