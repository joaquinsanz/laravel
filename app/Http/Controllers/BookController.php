<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Place;
use BooksTableSeeder;
use Illuminate\Foundation\Console\Presets\React;

class BookController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Book::class);
        $books = Book::paginate();
        $places = Place::all();
        return view('book.index', ['books'=>$books, 'places'=> $places]);
    }
    public function show($id, Request $request)
    {
        $user = \Auth::user();
        $book = Book::FindOrFail($id);
        //uso de autorización versatil
        if(!$user->can('view', $book)){
            return 'Altooo';
        }
        //uso genérico envia error 403
        $this->authorize('view', $book);
        //guardar ultimo libro visto en sesion
        $request->session()->put('lastbook', $book);
        $books = $request->session()->get('books');
        if (!$books){
            $books = array();
        }
        $books[] = $book;
        $request->session()->put('books', $books);
        
        return view('book.show', ['book'=>$book]);
    }
    public function create()
    {
        $places = Place::all();
        return view('book.create',['places'=>$places]);
    }
    public function edit($id)
    {
        $book = Book::find($id);
        $place = Place::find($book->place_id);
        $places = Place::all();
        return view('book.edit', ['book'=>$book,'place'=>$place, 'places'=>$places]);

    }
    public function update($id, Request $request)
    {
        $book = Book::find($id);
        $book->title = $request->title;
        $book->author= $request->author;
        //metodo rellenar todo a lo bestia
        //$book -> fill($request->all())
        $book->save();
        return redirect('/books/' . $book->$id);
    }
    public function destroy($id)
    {
        $book= Book::find($id);
        $this->authorize('delete', $book);
        $book->delete();
        //Book::destroy($id); //con el destroy puedes recibir un array de id para borrar (2,5,9)
        return back();
        // tambien podria ser con lo siguiente:
        /*
        $book = Book::find($id);
        $book->delete();
        return redirect('/books')
        */
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'place_id' => 'required|exists:places,id',
        ]);
        //como leer los parametros
        //dd($request->all());
        //dd($request->title);
        //dd($request->input('title'));
        

        //metodo 1
        // $book = new Book;
        // $book->title = $request->title;
        // $book->save();


        //metodo 2
        // $book = Book::create(['title'=> $request->title]);

        //metodo 3
        Book::create($request->all());

        return redirect('/books');
    }
    public function forget(Request $request)
    {
        $request->session()->forget('lastbook');
        $request->session()->forget('books');
        return back();
    }
}
