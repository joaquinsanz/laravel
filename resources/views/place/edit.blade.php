<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Editar</title>
</head>
<body>
    <h1>Edicion de lugares</h1>
    <form action="/places/{{  $place->id  }}" method="POST">
    @csrf
    <label for="name">Nombre</label><input type="text" name="name" value="{{ $place->name }}">
            <br>
            <br>
           
            <input type="hidden" name="_method" value="put">
            <input type="submit" value="Editar">
    </form>
</body>
</html>