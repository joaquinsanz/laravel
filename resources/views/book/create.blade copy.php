<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create</title>
</head>
<body>
    <h1>Alta de libro</h1>
    <form action="/books" method="POST">
        @csrf
        <label for="title">Titulo</label>
        <input type="text" name="title">
        <label for="author">Autor</label>
        <input type="text" name="author">
        <select name="place_id">
            @foreach ($places as $place)
                <option value={{$place->id}} {{$place->id == $book->place_id ? 'selected="selected"':''}}>
                    {{$place->name}}
                </option>
            @endforeach
        </select>
        {{$errors->first('place_id')}}
        <br>
        <input type="submit" value="nuevo">
    </form>
</body>
</html>