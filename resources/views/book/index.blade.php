@extends('layouts.app')

@section('content')
    <h1>Lista de libros</h1>
    
    El último libro es: 
    @if (Session::get('lastbook'))
        {{ Session::get('lastbook')->title }}
        ({{ Session::get('lastbook')->editorial->name }})
        <a href="/books/forget"> Olvidar último libro</a>
    @else
        Ninguno
    @endif
    <hr>
    Los libros visitrados son:
    @if (Session::get('books'))
        @foreach(Session::get('books') as $index => $book)
            <li>{{ $index }} - {{$book->title}}</li>
        @endforeach
    @else
        No has visto ninguno todavía
    @endif
    
    <br>
    <br>
    
    <h3>Books</h3>
  
    <h1> <a href="/books/create">Nuevo</a> </h1>
    
    <ul>
    
    @forelse($books as $book)

    <li>
        Titulo:      {{ $book->title }} 
        Autor :      {{ $book->author }} 
        Place :      {{ $book->place->name }} 
        Editorial:   {{ $book->editorial->name}}
        Cdu:         {{ $book->CDU->description}}

        <form action="/books/{{ $book->id }}" method="POST" >
            @can('view', $book)
            <a href="/books/{{ $book->id }}">Ver</a>
            @endcan
            <a href="/books/{{ $book->id }}/edit">Editar</a>
            @csrf
            @can('delete', $book)
            <input type="hidden" name="_method" value="delete">
            <input type="submit" value="Borrar">
            @endcan
        </form>
    </li>

    @empty
        No hay libros
        @endforelse
      
</ul>
    {{ $books->render() }}
    @endsection