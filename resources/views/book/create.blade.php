<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create</title>
</head>
<body>
    <h1>Alta de libro</h1>
    <form action="/books" method="POST">
        @csrf
        <label for="title">Titulo</label>
        <input type="text" name="title" value="{{ old('title') }}">
        {{ $errors->first('title') }}
        <label for="author">Autor</label>
        <input type="text" name="author">
        <label for="place_id">Sitio</label>
        <select name="place_id">
            @foreach ($places as $place)
                <option value={{$place->id}} {{$place->id == old('place_id') ? 'selected="selected"':''}}>
                    {{$place->name}}
                </option>
            @endforeach
        </select>
        {{ $errors->first('place_id') }}
        <br>
        <select name="cdu">
            @foreach ($cdus as $cdu)
                <option value={{$cdu->cdu}} {{$cdu->cdu == old('cdu') ? 'selected="selected"':''}}>
                    {{$cdu->name}}
                </option>
            @endforeach
        </select>
        {{ $errors->first('cdu') }}
        <br>
        <input type="submit" value="nuevo">
    </form>
    {{ dd(Session::all()) }}
</body>
</html>