<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Editar</title>
</head>
<body>
    <h1>Edicion de libro</h1>
    <form action="/books/{{  $book->id  }}" method="POST">
        @csrf
        <label for="title">Titulo</label>
        <input type="text" name="title" value="{{  $book->title  }}">
        <label for="author">Autor</label>
        <input type="text" name="author" value="{{  $book->author  }}">
        <select name="place_id">
            @foreach ($places as $place)
                <option value={{$place->id}} {{$place->id == $book->place_id ? 'selected="selected"':''}}>
                    {{$place->name}}
                </option>
            @endforeach
        </select>
        <input type="hidden" name="_method" value="put">
        <input type="submit" value="edit">
    </form>
</body>
</html>