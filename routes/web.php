<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/hola', function () {
    return 'Hola mundo';
});

Route::get('/hola/{name}', function ($name) {
    return "Hola $name";
});
    //Route::get('/hola', 'HolaController@holamundo');
    //Route::get('books', 'BookController@index');
    //Route::get('books/{id}', 'BookController@show');
Route::get('books/forget', 'BookController@forget');
Route::resource('books', 'BookController');
Route::resource('places', 'PlaceController');
Route::get('places/{id}/json', 'PlaceController@showJson');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
