<?php
use App\Editorial;
use Illuminate\Database\Seeder;

class EditorialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Editorial::create(['name' => 'Planeta']);
        Editorial::create(['name' => 'SM']);
        Editorial::create(['name' => 'Santillana']);
        Editorial::create(['name' => 'Anaya']);
    }
}
