<?php

use App\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Book:: create(['title' => 'El libro de la selva']);
       // Book:: create(['title' => 'Bamby']);
        
        factory(App\Book::class, 30)->create(); 
    }
}
