<?php

use Illuminate\Database\Seeder;
use App\Cdu;
class CdusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cdu::create(['cdu'=>'abc', 'description'=>'CDU abc']);
        Cdu::create(['cdu'=>'123', 'description'=>'CDU 123']);
        Cdu::create(['cdu'=>'321', 'description'=>'CDU 321']);
    }
}
